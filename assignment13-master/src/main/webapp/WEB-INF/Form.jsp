<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form:form action="userInfo" modelAttribute="emp">
		<label>
			Organisation Name
		</label>
		<form:input path="orgName" required="true" id="orgName"/>
		<form:errors path="orgName" cssClass="error"/>
		<label> 
			First Name
		</label>
	<form:input path="firstName" required="true" id="firstName"/>
	<label> 
		Last Name
	</label>
	<form:input path="lastName" required="true"  id="lastName"/>
	<input type="submit" value="SUBMIT">
</form:form>
</body>
</html>